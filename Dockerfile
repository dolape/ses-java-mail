FROM openjdk:8-jre-alpine
WORKDIR /
ADD target/ses-email-example-1.0-SNAPSHOT-jar-with-dependencies.jar ses-java-mail.jar
CMD java -jar ses-java-mail.jar